import numpy as np
import matplotlib.pyplot as plt

import csv
import datetime as date
import sys
from decimal import Decimal

## column names:
## "id_local","pair","amount","buy_order_id","sell_order_id","price","timestamp","type","id"

data = []
i = 0;

with open('transactions.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        if row[1] == "btc_usd":
            i = i+1
            sys.stdout.write("\033[F") #back to previous line
            sys.stdout.write("\033[K") #clear line
            print('loading transaction n°'+ str(i))
            d = {}
            d["price"] = row[5]
            d["time"] = date.datetime.fromtimestamp(int(row[6])).strftime('%Y-%m-%d %H:%M')
            data.append(d)

print("Done.\n\n>> transactions filtered by pair and loaded in memory with success \o/")
#print("\n>> raw data looks like this:")
#print(data)

dataLen = len(data)
#print("\n>> Price average per day calculation; data size: " + str(dataLen) +"\n")

print("\n>> Price average per day calculation; data size: " + str(dataLen) +"\n>> Initialisation...")

# pricesPerDay array
pricesPerDay = []
# matplotlib compatible data
pltTime = []
pltPrices = []
# tmp values for loop usage
time_tmp = data[0]["time"]
price_tmp = 0
i_tmp = 0       # temporary loop iterator (reset for each new day)
i = 0           # loop iterator
n_day = 0       # count of the days generated
clearLine = False
for d in data:
    i_tmp += 1
    print(">> [ "+str(round(Decimal((i+1)*100/dataLen),0)) + "% ] transaction of the day n°" + str(i_tmp))

    # new day or end of data
    if time_tmp != d["time"] or dataLen-1 == i:
        n_day += 1

        # pricesPerDay array
        dayPrice = {}
        dayPrice["time"] = time_tmp
        dayPrice["price"] = round(Decimal(price_tmp / i_tmp), 2)
        pricesPerDay.append(dayPrice)

        # matplotlib compatible data
        pltTime.append(time_tmp)
        pltPrices.append(round(Decimal(price_tmp / i_tmp), 2))

        sys.stdout.write("\033[F") #back to previous line
        sys.stdout.write("\033[K") #clear line
        print("new day added : " + str(dayPrice["time"]) + " | $" + str(dayPrice["price"]) + " | progress: " + str(i+1) + "/"+str(dataLen))

        # reset day data
        time_tmp = d["time"]
        i_tmp = 0
        price_tmp = 0
        clearLine = False
    else:
        price_tmp += float(d["price"])
        #print(price_tmp)
        clearLine = True

    if clearLine:
        sys.stdout.write("\033[F") #back to previous line
        sys.stdout.write("\033[K") #clear line
        clearLine = False
    i += 1

print("Done.\n")
#print("\nResult:")
#print(pricesPerDay)

print(">> Create file result.csv: ")
i = 0
with open('result.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for p in pricesPerDay:
        i += 1
        sys.stdout.write("\033[F") #back to previous line
        sys.stdout.write("\033[K") #clear line
        print(">> [ "+str(i*100/n_day)+" ] writting price $" + str(p["price"]) + " into result.csv")
        spamwriter.writerow([str(p["time"]), str(p["price"])])

#plt.plot(pltTime, pltPrices)
#plt.show()

print("\n>> script has been terminated without any problem :)\nquit.")
