import numpy as np
import matplotlib.pyplot as plt

import csv
import datetime as date
import sys
from decimal import Decimal

## column names:
## "id_local","pair","amount","buy_order_id","sell_order_id","price","timestamp","type","id"

time = []
price = []
i = 0;

with open('result.h.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        i = i+1
        if i >= 200:
            print('loading transaction n°'+ str(i) + ' ' + row[1])
            time.append(i)
            price.append(float(row[1]))

        if i > 280:
            break

#print(time)
plt.plot(time, price)
plt.show()

print("\n>> script has been terminated without any problem :)\nquit.")
